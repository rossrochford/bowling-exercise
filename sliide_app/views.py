from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.generic import View

import models


class GetTotal(View):

    def get(self, request, player_id, game_id):

        try:
            self.game = models.Game.objects.get(pk=game_id)
        except ObjectDoesNotExist:
            return JsonResponse({'success': False})

        if player_id != str(self.game.player.pk):
            return JsonResponse({'success': False})

        return JsonResponse({'success': True, 'total': self.game.points})


class RollView(View):

    def post(self, request, *args, **kwargs):

        game_id = request.POST.get('game_id')
        player_id = request.POST.get('player_id')
        pins_knocked = request.POST.get('pins_knocked')

        try:
            self.game = models.Game.objects.get(pk=game_id)
            self.player = User.objects.get(pk=player_id)
        except ObjectDoesNotExist:
            return JsonResponse({'success': False})  # maybe add 'reason' field?

        try:
            pins_knocked = [int(v) for v in pins_knocked.split(',') if v != '']
        except ValueError:
            return JsonResponse({'success': False})

        if not self._is_roll_valid(pins_knocked):
            return JsonResponse({'success': False})

        self.roll(pins_knocked)

        return JsonResponse({'success': True})

    def roll(self, pins_knocked):

        prev_round, current_round = self._get_curr_prev_rounds()

        if current_round is None:
            round_num = prev_round.round_num + 1 if prev_round else 1
            current_round = models.Round(
                game=self.game, round_num=round_num, round_type='in_progress'
            )
            current_round.save()

        current_round.roll_num += 1
        current_round.pins_down_list = current_round.pins_down_list + pins_knocked

        self.game.points += len(pins_knocked)

        # add bonus
        prev_round_type = prev_round.round_type if prev_round else None
        if prev_round_type in ('spare', 'strike') and current_round.roll_num == 1:
            self.game.points += len(pins_knocked)
        elif prev_round_type == 'strike' and current_round.roll_num == 2:
            self.game.points += len(pins_knocked)

        if current_round.finished:
            if len(current_round.pins_down_list) < 10:
                current_round.round_type = 'normal'
            elif current_round.roll_num == 1:
                current_round.round_type = 'strike'
            else:
                current_round.round_type = 'spare'

        current_round.save()
        self.game.save()

    def _is_roll_valid(self, pins_knocked):

        if len(pins_knocked) != len(set(pins_knocked)):
            return False  # duplicates in pins_knocked

        invalid_pins = [p for p in pins_knocked if p not in range(10)]
        if invalid_pins:
            return False

        prev_round, current_round = self._get_curr_prev_rounds()

        if current_round is None and (prev_round is None or prev_round.round_num < 10):
            # this must be the first roll in the next round, so no need to validate any further
            return True

        # game over
        if prev_round and prev_round.round_num == 10 and prev_round.finished:
            return False

        # attempted to knock down pins more than once, must be invalid
        if len(set(pins_knocked).intersection(current_round.pins_down_list)) > 0:
            return False

        return True

    def _get_curr_prev_rounds(self):
        """Get current and previous rounds and cache them"""
        if not (hasattr(self, 'current_round') and hasattr(self, 'prev_round')):
            current_round = self.game.round_set.filter(round_type='in_progress')
            self.current_round = current_round[0] if current_round else None
            self.prev_round = self.game.round_set.exclude(
                round_type='in_progress'
            ).order_by('round_num').last()

        return self.prev_round, self.current_round