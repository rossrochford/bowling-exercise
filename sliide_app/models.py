from django.contrib.auth.models import User
from django.db import models


class Game(models.Model):
    points = models.IntegerField(blank=False, default=0)
    player = models.ForeignKey(User, blank=False)

    start_date = models.DateTimeField(blank=False, auto_now_add=True)

    # @property
    # def is_game_over(self):
    #     latest_round = self.round_set.order_by('round_num').last()
    #     #prev_round = game.round_set.exclude(round_type='in_progress').order_by('round_num').last()
    #     if latest_round and latest_round.round_num == 10 and latest_round.round_type != 'in_progress':
    #         return True
    #     return False

    def get_prev_round(self):
        return self.round_set.order_by('round_num').last()

ROUND_TYPES = (
    ('in_progress', 'In Progress'),
    ('normal', 'Normal'),
    ('spare', 'Spare'),
    ('strike', 'Strike'),
)


class Round(models.Model):
    game = models.ForeignKey('Game')
    round_num = models.IntegerField(blank=False)
    roll_num = models.IntegerField(blank=False, default=0)
    round_type = models.CharField(blank=False, choices=ROUND_TYPES, default='in_progress', max_length=10)
    pins_down = models.CommaSeparatedIntegerField(blank=True, max_length=40)

    @property
    def pins_down_list(self):
        return [int(p) for p in self.pins_down.split(',') if p != '']

    @pins_down_list.setter
    def pins_down_list(self, vals):
        self.pins_down = ','.join(str(v) for v in vals)

    @property
    def finished(self):
        if self.round_num < 9 and self.roll_num == 2:
            return True
        if self.round_num == 10 and self.round_num == 3:
            return True
        if len(self.pins_down_list) == 10:
            return True
        return False


# class Player(models.Model):
#     user = models.OneToOneField(User, blank=True)
#
#
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Player.objects.create(user=instance)
#
#
# post_save.connect(create_user_profile, sender=User)