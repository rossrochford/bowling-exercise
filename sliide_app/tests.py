from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import RequestFactory

from sliide_app import models as m
from sliide_app import views

#testing: curl --data "game_id=1&player_id=1&pins_knocked=1,4,5" 127.0.0.1:8000/roll/


class RollTests(TestCase):

    def setUp(self):
        self.user = User(username='test user')
        self.user.save()

        self.game = m.Game(player=self.user)
        self.game.save()

        self.factory = RequestFactory()

    def _do_roll(self, pins):
        pins = ','.join(str(v) for v in pins)
        request = self.factory.post(
            '/roll/', {
                'game_id': self.game.pk, 'player_id': self.user.pk,
                'pins_knocked': pins
            }
        )
        return views.RollView.as_view()(request)

    def _do_strike(self, num_strikes=1):
        for r in range(num_strikes):
            self._do_roll(range(10))

    def test_first_roll(self):

        pins_knocked = [1, 3, 4]
        self._do_roll(pins_knocked)

        self.assertEqual(len(self.game.round_set.all()), 1)

        first_round = self.game.round_set.all()[0]

        self.assertEqual(first_round.round_num, 1)
        self.assertEqual(first_round.round_type, 'in_progress')
        self.assertEqual(first_round.pins_down_list, pins_knocked)

    def test_empty_roll(self):

        resp = self._do_roll([])

        first_round = self.game.round_set.all()[0]

        self.assertEqual(first_round.pins_down_list, [])

    def test_strike_roll(self):

        self._do_strike()

        first_round = self.game.round_set.all()[0]

        self.assertEqual(first_round.round_type, 'strike')
        self.assertEqual(first_round.pins_down_list, range(10))

    def test_second_normal_roll(self):

        resp = self._do_roll([1, 3, 4])
        resp = self._do_roll([2])

        self.assertEqual(len(self.game.round_set.all()), 1)

        first_round = self.game.round_set.all()[0]

        self.assertEqual(first_round.round_num, 1)
        self.assertEqual(first_round.round_type, 'normal')
        self.assertItemsEqual(first_round.pins_down_list, [1, 2, 3, 4])
    #
    def test_second_roll_spare(self):

        resp = self._do_roll([1, 3, 4])
        resp = self._do_roll([0, 2, 5, 6, 7, 8, 9])

        self.assertEqual(len(self.game.round_set.all()), 1)

        first_round = self.game.round_set.all()[0]

        self.assertEqual(first_round.round_num, 1)
        self.assertEqual(first_round.round_type, 'spare')
        self.assertItemsEqual(
            first_round.pins_down_list, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        )

    def test_roll_after_spare(self):
        self._do_roll([1, 3, 4])
        self._do_roll([0, 2, 5, 6, 7, 8, 9])
        pins = [0, 1]
        self._do_roll(pins)

        game = m.Game.objects.all()[0]
        self.assertEqual(game.points, 10 + (2 * len(pins)))

        self._do_roll([2,3])
        game = m.Game.objects.all()[0]
        self.assertEqual(game.points, 10 + (2 * len(pins)) + 2)  # no bonus

    def test_roll_after_strike(self):
        self._do_strike()
        pins = [0, 1]
        self._do_roll(pins)

        self.assertEqual(len(self.game.round_set.all()), 2)

        first_round = self.game.round_set.all()[0]
        second_round = self.game.round_set.all()[1]

        self.assertEqual(first_round.round_type, 'strike')
        self.assertEqual(second_round.round_type, 'in_progress')
        self.assertEqual(second_round.roll_num, 1)

        # the 'game' model instance doesn't get updated so we need to
        # reload it from the database
        game = m.Game.objects.all()[0]
        # test that the bonus has been added
        self.assertEqual(game.points, 10 + (2 * len(pins)))

        self._do_roll([2,3])
        game = m.Game.objects.all()[0]
        self.assertEqual(game.points, 10 + (2 * len(pins)) + 2 * 2)  # with bonus

    def test_final_round_allows_three_rolls(self):

        self._do_strike(num_strikes=9)

        self.assertEqual(len(self.game.round_set.all()), 9)

        self._do_roll([0])
        self._do_roll([1])
        resp = self._do_roll([2, 3, 4, 5, 6, 7, 8, 9])

        self.assertEqual(len(self.game.round_set.all()), 10)

        prev_round = self.game.get_prev_round()

        self.assertEqual(prev_round.roll_num, 3)
        self.assertEqual(prev_round.round_type, 'spare')

    def test_roll_fails_after_game_over(self):
        self._do_strike(num_strikes=10)
        resp = self._do_roll([0])
        self.assertEqual(resp.content, '{"success": false}')

