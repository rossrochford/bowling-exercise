# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sliide_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='round',
            name='round_type',
            field=models.CharField(default=b'in_progress', max_length=10, choices=[(b'in_progress', b'In Progress'), (b'normal', b'Normal'), (b'spare', b'Spare'), (b'strike', b'Strike')]),
        ),
    ]
