# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('points', models.IntegerField(default=0)),
                ('start_date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=80)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Round',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('round_num', models.IntegerField()),
                ('roll_num', models.IntegerField(default=0)),
                ('round_type', models.IntegerField(default=0, choices=[(b'in_progress', b'In Progress'), (b'normal', b'Normal'), (b'spare', b'Spare'), (b'strike', b'Strike')])),
                ('pins_down', models.CommaSeparatedIntegerField(max_length=40, blank=True)),
                ('game', models.ForeignKey(to='sliide_app.Game')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='game',
            name='player',
            field=models.ForeignKey(to='sliide_app.Player'),
            preserve_default=True,
        ),
    ]
