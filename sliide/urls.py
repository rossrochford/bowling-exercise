from django.conf.urls import patterns, include, url
from django.contrib import admin

from sliide_app import views


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^roll/', views.RollView.as_view()),
    url(r'^get-total/(?P<player_id>\d{1,5})/(?P<game_id>\d{1,5})/$', views.GetTotal.as_view())
)